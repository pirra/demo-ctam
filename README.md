*Nota: este wiki utiliza el lenguaje de lectura y escritura para publicación web: [Markdown](https://0xacab.org/help/user/markdown)*

**Índice**

[[_TOC_]]

## Esta podría ser la página de bienvenida para CTAM
Se trata de un **wiki**, por tanto puede generar vínculos a otras páginas fácilmente. Y de ese modo estructurar la información por ejemplo del siguiente modo

### Organización
Podría contar con páginas
* de cada **organización participante**,
* para cada **proyecto** desarrollado por cada organización
* de cada **grupo de trabajo** temático

### Documentación
Así como -desde luego-, para toda la **documentación técnica** que éstos generan, relativa a los productos de la implementación: guías y manuales de procedimientos, presentaciones, etc.

### Implementación
Teniendo presente que cada etapa de un proyecto da pie a interacciones entre enlaces y coordinadores que quedan documentadas en issues o **incidencias** si se ha configurado el uso de Español.
En el menú del panel izquierdo encontramos *Incidencias*, y ahí varias opciones para conocer la condición que guarda el conjunto de incidencias (Lista, Tableros, Etiquetas e Hitos)

Carlos y Pirra, ¿podrían, por favor, ayudarme a responder las siguientes preguntas?:
* *¿Qué herramientas tiene GitLab para realizar [consultas](https://trac.frentenorte.net/CTAM/query)?*
* *¿Cómo podría recoger el [sistema de Incidencias](https://0xacab.org/pirra/demo-ctam/-/issues/new) la estructura (que se muestra más abajo) para organizar la relación que guarda cada incidencia con el proyecto y grupo de trabajo que le corresponde?*

### Estructura organizativa
La configuración actual contempla la existencia de siete grupos de trabajo con las siguientes personas coordinadoras:

|  | grupo | coordinador-a |
| ------ | ------ | ------ |
| 1 | [CiviCRM - Gestión de contactos](/civicrm.md) | martha |
| 2 | [Debian - Sistema de escritorio](/debian.md) | tomas |
| 3 | [GIS - Geomática](/geomatica.md) | alonso |
| 4 | [Streaming - Transmisiones](/streaming.md) | atitlan |
| 5 | [Wordpress - Desarrollo web](/wordpress.md) | ingrid |
| 5 | [Anonimato en la red](/anonimato.md) | pirra |
| 6 | [Backups - Respaldos](/respaldos.md) | carlos |

### Otras páginas wiki de prueba
 * [Página del Grupo de trabajo CiviCRM](/civicrm.md)
 * Crea o edita [nuevas páginas](/NuevasPaginas.md)

